<html>
<head>
</head>
<body>
<?php
require("info.php");

setlocale(LC_ALL, 'ja_JP.UTF-8');
$file = '../csv/k30.csv';
$jsonfile = '../json/mapdata.json';
$data = file_get_contents($file);
$temp = tmpfile();
 
fwrite($temp, $data);
rewind($temp);

$mapdata_array = array();

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
	$text = $data[4]." ".$data[5];
	$pattern = '/class="V(\d+)"/';
	preg_match_all($pattern,$data[4],$regex);
	$count = array_count_values($regex[1]);
	arsort($count);
	$class = array_keys($count);
	$class = $class[0];
	
	$mapdata_array[] = array(
		'type'=> "Feature",
		'geometry' => array('type' => "Point", 'coordinates' => array( (double)$data[3],  (double)$data[2])),
		'properties' => array(
			'id' => $data[0],
			'date' => date("Y/m/d",strtotime($data[1])),
			'text' => $data[4],
			'back' => $data[5],
			'class' => array($data[6], $data[7], $data[8]),
			'note' => array($data[9], $data[10], $data[11], $data[12], $data[13], $data[14], $data[15], $data[16]),
			'photo' => array($data[17], $data[18],$data[19], $data[20], $data[21], $data[22], $data[23], $data[24],$data[25], $data[26], $data[27], $data[28]),
			'topic' => (int)$class
		)
	);
}
fclose($temp);

$whole_array = array(
	'type'=> 'FeatureCollection',
	'features'=> $mapdata_array
);
$json_value	= json_encode($whole_array);
$json_value = str_replace("}},","}},\n",$json_value);

$handle = fopen($jsonfile, 'w');
if(fwrite($handle, $json_value) === FALSE){
	echo ("GeoJsonデータの登録に失敗しました。");
}
else{
	echo ("GeoJsonデータの登録に成功しました！");
}
fclose($handle);

?>
</body></html>