/**
 * Created by Yurina on 2016/01/27.
 */
var gulp = require("gulp");
var plumber = require("gulp-plumber");
var connect = require('gulp-connect');
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var jade = require('gulp-jade');
var uglify = require("gulp-uglify");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var cleanCSS = require('gulp-clean-css');

gulp.task("sass", function() {
    gulp.src(["develop/sass/**/*.scss", '!develop/sass/**/_*.scss'])
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest("./app/css"))
        .pipe(connect.reload());
});

gulp.task('jade', function () {
    gulp.src(['develop/jade/**/*.jade','!develop/jade/**/_*.jade'])
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('./app'))
        .pipe(connect.reload());
});

gulp.task('browserify', function (){
    browserify({
        entries: ['develop/js/main.js']
    })
        .bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest('./app/js'))
        .pipe(connect.reload());

});

gulp.task("watch", function() {
    gulp.watch("develop/sass/**/*.scss",["sass"]);
    gulp.watch("develop/jade/**/*.jade",["jade"]);
    gulp.watch("develop/js/main.js", ["browserify"]);
});

gulp.task('connect', function() {
    connect.server({
        root: './',
        livereload: true
    });
});

gulp.task('default', ['connect', 'watch']);


/*---------------
 minify
 ----------------*/
gulp.task('uglify', function (){
    gulp.src(["./app/js/*.js"])
        .pipe(uglify())
        .pipe(gulp.dest("./app/js"));
    gulp.src('./node_modules/cesium/Build/CesiumUnminified/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./app/Cesium'));
});

gulp.task('minify-css', function() {
    gulp.src('./app/css/**/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./app/css'));
    gulp.src('./node_modules/cesium/Build/CesiumUnminified/**/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./app/Cesium'));
    gulp.src('./node_modules/sweetalert/dist/sweetalert.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./app/css'));
});
