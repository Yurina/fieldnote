/**
 * Created by user on 15/04/24.
 */
var viewer = new Cesium.Viewer('cesiumContainer');

//Example 1: Load with default styling.
Sandcastle.addDefaultToolbarButton('Default styling', function() {
    viewer.dataSources.add(Cesium.GeoJsonDataSource.load('json/ne_10m_us_states.topojson'));
});