/**
 * Created by Yurina on 2016/06/10.
 */

window.jQuery = require('jquery');
window.$ = window.jQuery;

require('./JapanGSITerrainProvider');
require('./info');
require('sweetalert');

var viewerMain;
var viewerMini;

(function() {
    "use strict";
    //Sandcastle_Begin
    viewerMain = new Cesium.Viewer('cesiumContainer', {
        infoBox: true, //Disable InfoBox widget
        selectionIndicator: true, //Disable selection indicator
        animation: false,
        mapProjection: new Cesium.WebMercatorProjection(Cesium.Ellipsoid.WGS84)
    });

    viewerMini = new Cesium.Viewer('cesiumContainerMini', {
        infoBox: true, //Disable InfoBox widget
        selectionIndicator: true, //Disable selection indicator
        animation: false,
        mapProjection: new Cesium.WebMercatorProjection(Cesium.Ellipsoid.WGS84),
        timeline: false
    });

    //Enable lighting based on sun/moon positions
    viewerMain.scene.globe.enableLighting = true;
    viewerMini.scene.globe.enableLighting = true;

    //Use STK World Terrain
    viewerMain.terrainProvider = new Cesium.JapanGSITerrainProvider({});
    viewerMini.terrainProvider = new Cesium.JapanGSITerrainProvider({});

    Cesium.BingMapsApi.defaultKey = 'Apnx5lf7nHOA_8mYeds0TzNTseHFn5-_s0WsbU7O5M9Eh3ygcbzWlNhbnKkPmcuc';
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(139.668866, 35.720293, 139.911045, 35.581014);
    Cesium.Camera.DEFAULT_VIEW_FACTOR = 0;

    var entitiesMain;
    var entitiesMini;
    var keyword = [];
    var topicWord = [];
    var searchMultiple = false;
    var searchAnd = true;
    var initDateStart;
    var initDateStop;
    var intervalDate;
    var sliderA;
    var sliderB;
    var iconMainSize = 0.7;
    var iconMiniSize = 0.2;

    setMapColor(viewerMain.scene.imageryLayers);
    setMapColor(viewerMini.scene.imageryLayers);

    flyToFirstPos();
    loadJson(viewerMain);
    loadJson(viewerMini);
    $('#loadingOverlay').style.display = "block";
    initWindowSize();
    addToolbar();

    function addToolbar(){
        var button = document.createElement("button");
        button.type = "button";
        button.className = "cesium-button cesium-toolbar-button";
        button.setAttribute("data-bind","attr: { title: tooltip },click: command,cesiumSvgPath: { path: _svgPath, width: 28, height: 28 }");
        button.setAttribute("onClick", "setMapColor(viewerMain.scene.imageryLayers)");

        var img = document.createElement("img");
        img.src = "res/black_map.png";
        button.appendChild(img);

        var obj = document.getElementsByClassName("cesium-viewer-toolbar");
        obj[0].appendChild(button);
    }

    /**
     * マップのデザイン
     * @imageryLayers
     */
    function setMapColor(imageryLayers){
        var layer = imageryLayers.get(0);
        layer.saturation = 0;
        layer.brightness = 0.2;
        layer.contrast = 1.68;
        layer.hue = 0;
        layer.gamma = 2.3;
    }

    /**
     * ヘッダーウィンドウの高さ調整
     */
    function initWindowSize(){
        document.getElementById("collapse_content_search").style.height = document.documentElement.clientHeight - 450 + "px";

        var resizeTimer;
        var interval = Math.floor(1000 / 60 * 10);

        window.addEventListener('resize', function (event) {
            if (resizeTimer !== false) {
                clearTimeout(resizeTimer);
            }
            resizeTimer = setTimeout(function () {
                if(document.getElementById("collapse_content_timeline").style.display == "none") {
                    document.getElementById("collapse_content_search").style.height = document.documentElement.clientHeight - 270 + "px";
                }
                else{
                    document.getElementById("collapse_content_search").style.height = document.documentElement.clientHeight - 450 + "px";
                }
            }, interval);
        });
    }

    function finishLoading(){
        fadeOut("loadingOverlay", 100);
    }


    /**
     * 初期カメラ位置
     */
    function flyToFirstPos() {
        var altitude = 7 * Math.pow(10, 6);
        viewerMain.camera.flyTo({
            destination : Cesium.Cartesian3.fromDegrees(115.0, 5.0, altitude)
        });
    }


    /**
     * マップデータの読み込み
     */
    function loadJson(viewer) {
        var promise = Cesium.GeoJsonDataSource.load('res/json/mapdata.json');
        promise.then(function (dataSource) {
            viewer.dataSources.add(dataSource);
            var entities;
            if(viewer === viewerMain) {
                entitiesMain = dataSource.entities.values;
                entities = entitiesMain;
            }else{
                entitiesMini = dataSource.entities.values;
                entities = entitiesMini;
            }

            var size = 0.7;
            if(viewer === viewerMini) {
                size = 0.2;
            }

            for (var i = 0; i < entities.length; i++) {
                var entity = entities[i];

                if (entity.billboard) {
                    if (entity.properties.topic != 0) {
                        var color = Cesium.Color.fromCssColorString(
                            topic[entity.properties.topic]).withAlpha(0.5);
                        entity.billboard.color = color;
                        entity.billboard.image = new Cesium.ConstantProperty("res/icon.png");
                        entity.billboard.scale = size;
                    }
                    else {
                        entity.billboard.color = Cesium.Color.WHITE.withAlpha(0.5);
                        entity.billboard.image = new Cesium.ConstantProperty("res/icon.png");
                        entity.billboard.scale = size;
                    }
                    entity.billboard.verticalOrigin = Cesium.VerticalOrigin.CENTER;
                    entity.billboard.show = true;
                }
            }

            if(viewer === viewerMain) {
                setInitTimeline();
                setSelectBox(entities);
                finishLoading();
                search(entities.length);
                setClickEvent();
                setCameraListener();
                document.onkeydown = keyevent;
            }

        }).otherwise(function (error) {
        });
    }



    /**
     * タイムラインテーブル作成
     */
    function createTable(i, iPosition, current){
        var tableContent = "";
        loop: for(var j = 0; j < 7; j++){

            var k = 0;

            if(iPosition == 6){
                k  = 1;
                for(var a = 0; a < iPosition - j + 1; a++) {
                    k--;
                    if(i - iPosition + j < 0){
                        continue loop;
                    }
                    while(entitiesMain[i + k].billboard.show.getValue(viewerMain.clock.currentTime) == false) {
                        if(i + k - 1 < 0 || entitiesMain.length < i + 1 + k - 1){
                            continue loop;
                        }
                        k--;
                    }
                }
                tableContent += '<tr class="list" id="' + (i + k) + '" onclick="tableClick(this);">';
            }
            else if(iPosition == 0){
                k = -1;
                for(var a = 0; a < j - iPosition + 1; a++) {
                    k++;
                    if(entitiesMain.length < i - iPosition + j + 1){
                        continue loop;
                    }
                    while(entitiesMain[i + k].billboard.show.getValue(viewerMain.clock.currentTime) == false) {
                        if(i + k + 1 < 0 || entitiesMain.length < i + 1 + k + 1){
                            continue loop;
                        }
                        k++;
                    }
                }
                tableContent += '<tr class="list" id="' + (i + k) + '" onclick="tableClick(this);">';
            }
            else if(j == iPosition && current){
                tableContent += '<tr class="list-current" id="' + i + '" onclick="tableClick(this);">';
            }
            else if(j < iPosition && current){
                k  = 0;
                for(var a = 0; a < iPosition - j; a++) {
                    k--;
                    if(i - iPosition + j < 0){
                        continue loop;
                    }
                    while(entitiesMain[i + k].billboard.show.getValue(viewerMain.clock.currentTime) == false) {
                        if(i + k - 1 < 0 || entitiesMain.length < i + 1 + k - 1){
                            continue loop;
                        }
                        k--;
                    }
                }
                tableContent += '<tr class="list" id="' + (i + k) + '" onclick="tableClick(this);">';
            }
            else if(j > iPosition && current){
                k = 0;
                for(var a = 0; a < j - iPosition; a++) {
                    k++;
                    if(entitiesMain.length < i - iPosition + j + 1){
                        continue loop;
                    }
                    while(entitiesMain[i + k].billboard.show.getValue(viewerMain.clock.currentTime) == false) {
                        if(i + k + 1 < 0 || entitiesMain.length < i + 1 + k + 1){
                            continue loop;
                        }
                        k++;
                    }
                }
                tableContent += '<tr class="list" id="' + (i + k) + '" onclick="tableClick(this);">';
            }

            var type = entitiesMain[i + k].properties.class;
            if(type.indexOf("other") > -1){
                tableContent += '<td><img src="res/icon/other.png" width="15px"></td>';
            }
            else if(type.indexOf("note") > -1 && type.indexOf("hear") > -1 && type.indexOf("sum") > -1){
                tableContent += '<td><img src="res/icon/notehearsum.png" width="15px"></td>';
            }
            else if(type.indexOf("note") > -1 && type.indexOf("hear") > -1){
                tableContent += '<td><img src="res/icon/notehear.png" width="15px"></td>';
            }
            else if(type.indexOf("note") > -1 && type.indexOf("sum") > -1){
                tableContent += '<td><img src="res/icon/notesum.png" width="15px"></td>';
            }
            else if(type.indexOf("hear") > -1 && type.indexOf("sum") > -1){
                tableContent += '<td><img src="res/icon/hearsum.png" width="15px"></td>';
            }
            else if(type.indexOf("hear") > -1){
                tableContent += '<td><img src="res/icon/hear.png" width="15px"></td>';
            }
            else if(type.indexOf("note") > -1){
                tableContent += '<td><img src="res/icon/note.png" width="15px"></td>';
            }
            else if(type.indexOf("sum") > -1){
                tableContent += '<td><img src="res/icon/sum.png" width="15px"></td>';
            }
            else if(type.indexOf("view") > -1){
                tableContent += '<td><img src="res/icon/view.png" width="15px"></td>';
            }
            else if(type.indexOf("loc") > -1){
                tableContent += '<td><img src="res/icon/loc.png" width="15px"></td>';
            }
            else{
                tableContent += '<td><img src="res/icon/view.png" width="15px"></td>';
            }

            tableContent += '<td><div class="list-text">' + entitiesMain[i + k].properties.text + '</div></td></tr>';
        }
        if(tableContent != "") {
            document.getElementById("list_table").innerHTML = tableContent;
            if(iPosition == 6 && current){
                var nodes = document.getElementsByClassName("list");
                var element = nodes[nodes.length - 1];
                element.className = "list-current";
                viewerMain.selectedEntity = entitiesMain[element.id];
            }
            else if(iPosition == 0 && current){
                var nodes = document.getElementsByClassName("list");
                var element = nodes[0];
                element.className = "list-current";
                viewerMain.selectedEntity = entitiesMain[element.id];
            }
        }

    }


    /**
     * 初期タイムライン設定
     */
    function setInitTimeline() {
        //Set bounds of our simulation time
        var dateStrStart = entitiesMain[0].properties.date + " GMT";//UTCタイムにする
        var dateParseStart = Date.parse(dateStrStart);
        var dateStrStop = entitiesMain[entitiesMain.length - 1].properties.date + " GMT";//UTCタイムにする
        var dateParseStop = Date.parse(dateStrStop);
        initDateStart = Cesium.JulianDate.fromDate(new Date(dateParseStart));
        initDateStop = Cesium.JulianDate.addDays(Cesium.JulianDate.fromDate(new Date(dateParseStop)), 1, new Cesium.JulianDate());

        setZoom(initDateStart, initDateStop);
        setTimelineBar(dateParseStart, dateParseStop);
    }

    /**
     * フッタータイムラインの範囲設定
     * @param start
     * @param stop
     */
    function setZoom(start, stop){
        //Make sure viewer is at the desired time.
        viewerMain.clock.startTime = start.clone();
        viewerMain.clock.stopTime = stop.clone();
        viewerMain.clock.currentTime = start.clone();
        viewerMain.clock.clockRange = Cesium.ClockRange.LOOP_STOP; //Loop at the end
        viewerMain.clock.multiplier = 3600 * 24;//一秒あたり一日経過のスピード
        viewerMain.clock.shouldAnimate = false;

        viewerMain.clock.onTick.addEventListener(tickListener);

        //Set timeline to simulation bounds
        viewerMain.timeline.zoomTo(start, stop);
    }

    /**
     * ヘッダーのタイムラインスライダーに合わせてデータの絞り込み
     * @param start
     * @param stop
     */
    function limitData(start, stop){
        var count = 0;
        loop: for(var i = 0; i < entitiesMain.length; i++) {
            var dateParse = Date.parse(entitiesMain[i].properties.date + "GMT");
            var time = Cesium.JulianDate.fromDate(new Date(dateParse));
            if (time < start || stop < time) {
                entitiesMain[i].billboard.show = false;
                entitiesMini[i].billboard.show = false;
                continue;
            }
            else{
                if (keyword.length != 0) {
                    //AND
                    if (Array.isArray(keyword[0])) {
                        var text = entitiesMain[i].properties.text;
                        var topic = entitiesMain[i].properties.topic;
                        //topic検索を含む場合
                        for(var k = 0; k < topicWord.length; k++){
                            if (topic != topicWord[k]) {
                                entitiesMain[i].billboard.show = false;
                                entitiesMini[i].billboard.show = false;
                                continue loop;
                            }
                        }
                        loop2: for (var k = 0; k < keyword.length; k++) {
                            for (var j = 0; j < keyword[k].length; j++) {
                                if (text.match(keyword[k][j])) {
                                    if (k == keyword.length - 1) {
                                        entitiesMain[i].billboard.show = true;
                                        entitiesMini[i].billboard.show = true;
                                        count++;
                                        continue loop;
                                    }
                                    continue loop2;
                                }
                            }
                            break;
                        }
                        entitiesMain[i].billboard.show = false;
                        entitiesMini[i].billboard.show = false;
                    }
                    //OR
                    else {
                        var text = entitiesMain[i].properties.text;
                        var topic = entitiesMain[i].properties.topic;
                        //topic検索を含む場合
                        for(var j = 0; j < topicWord.length; j++){
                            if (topic == topicWord[j]) {
                                entitiesMain[i].billboard.show = true;
                                entitiesMini[i].billboard.show = true;
                                count++;
                                continue loop;
                            }
                        }
                        for (var j = 0; j < keyword.length; j++) {
                            if (text.match(keyword[j])) {
                                entitiesMain[i].billboard.show = true;
                                entitiesMini[i].billboard.show = true;
                                count++;
                                continue loop;
                            }
                        }
                        entitiesMain[i].billboard.show = false;
                        entitiesMini[i].billboard.show = false;
                    }
                }
                else if(topicWord.length != 0){
                    //AND検索
                    if(searchAnd){
                        for(var k = 0; k < topicWord.length; k++){
                            if (entitiesMain[i].properties.topic != topicWord[k]) {
                                entitiesMain[i].billboard.show = false;
                                entitiesMini[i].billboard.show = false;
                                continue loop;
                            }
                        }
                        entitiesMain[i].billboard.show = true;
                        entitiesMini[i].billboard.show = true;
                        count++;
                        continue loop;
                    }
                    //OR検索
                    else {
                        for (var k = 0; k < topicWord.length; k++) {
                            if (entitiesMain[i].properties.topic != topicWord[k]) {
                                continue;
                            }
                            else {
                                entitiesMain[i].billboard.show = true;
                                entitiesMini[i].billboard.show = true;
                                count++;
                                continue loop;
                            }
                        }
                        entitiesMain[i].billboard.show = false;
                        entitiesMini[i].billboard.show = false;
                    }
                }
                else{
                    entitiesMain[i].billboard.show = true;
                    entitiesMini[i].billboard.show = true;
                    count++;
                }
            }
        }
        search(count);
    }

    /**
     * タイムスパンをentityごとに設定
     * @param interval
     */
    function setIntervals(interval) {
        var dateStrStart = entitiesMain[0].properties.date + "GMT";
        var dateParseStart = Date.parse(dateStrStart);
        var tmpDate = dateParseStart;
        var oneDayArray = new Array();
        var index = 0;

        for(var i = 0; i < entitiesMain.length; i++){
            var dateParse = Date.parse(entitiesMain[i].properties.date + "GMT");

            if(dateParse != tmpDate){
                //前日分をpush
                for(var j = 0; j < oneDayArray.length; j++){
                    setDayRecord(index, oneDayArray, tmpDate, j);
                }
                //新規に当日分を作成
                oneDayArray = new Array();
                tmpDate = dateParse;
                index = i;
            }
            oneDayArray.push(entitiesMain[i]);

            //最終日分をpush
            if(i == entitiesMain.length - 1){
                for(var j = 0; j < oneDayArray.length; j++){
                    setDayRecord(index, oneDayArray, tmpDate, j);
                }
            }
        }

        function setDayRecord(from, records, fromDate, index) {
            var perSeconds = (60 * 60 * 24) / records.length;
            var time = Cesium.JulianDate.addSeconds(Cesium.JulianDate.fromDate(new Date(fromDate)), perSeconds * index, new Cesium.JulianDate());
            var position = records[index].position.getValue(viewerMain.clock.currentTime);
            if(position == null){
                return;
            }
            if(interval) {
                var timeInterval = new Cesium.TimeInterval({
                    start: time,
                    stop: Cesium.JulianDate.addDays(time, interval, new Cesium.JulianDate()),
                    isStartTimeIncluded: true,
                    isStopTimeIncluded: false,
                    data: position
                });
            }
            else{
                var timeInterval = new Cesium.TimeInterval({
                    start: initDateStart,
                    stop: initDateStop,
                    isStartTimeIncluded: true,
                    isStopTimeIncluded: false,
                    data: position
                });
            }

            entitiesMain[from + index].availability = new Cesium.TimeIntervalCollection();
            entitiesMain[from + index].availability.addInterval(timeInterval);
        }
    }


    /**
     * タイムスパン設定のセレクトボックスの設定、及びリスナ
     * @param entities
     */
    function setSelectBox(entities){
        var dateStrStart = entities[0].properties.date + "GMT";//UTCタイムにする
        var dateParseStart = Date.parse(dateStrStart);
        var startDate = new Date(dateParseStart);
        var dateStrStop = entities[entities.length - 1].properties.date + "GMT";//UTCタイムにする
        var dateParseStop = Date.parse(dateStrStop);
        var stopDate = new Date(dateParseStop);
        //Add a combo box for selecting each interpolation mode.
        addToolbarMenu([{
            text : '全表示',
            onselect : function() {
                removeTimeSpanBar();
                setIntervals(null);
            }
        }, {
            text : '日単位',
            onselect : function() {
                setTimeSpanBar(7, "DATE");
            }
        }, {
            text : '週単位',
            onselect : function() {
                setTimeSpanBar(4, "WEEK");
            }
        }, {
            text : '月単位',
            onselect : function() {
                setTimeSpanBar(12, "MONTH");
            }
        }, {
            text : '年単位',
            onselect : function() {
                setTimeSpanBar(stopDate.getFullYear() - startDate.getFullYear() - 1, "YEAR");
            }
        }], 'interpolationMenu');

        var defaultAction;
        function addToolbarMenu(options, toolbarID){
            var menu = document.createElement('select');
            menu.className = 'cesium-button';
            menu.onchange = function() {
                var item = options[menu.selectedIndex];
                if (item && typeof item.onselect === 'function') {
                    item.onselect();
                }
            };
            document.getElementById('toolbar').appendChild(menu);

            if (!defaultAction && typeof options[0].onselect === 'function') {
                defaultAction = options[0].onselect;
            }

            for (var i = 0, len = options.length; i < len; ++i) {
                var option = document.createElement('option');
                option.textContent = options[i].text;
                option.value = options[i].value;
                menu.appendChild(option);
            }
        }
    }

    /**
     * ヘッダーのタイムラインの設置、及びリスナの設定
     * @param startInt
     * @param stopInt
     */
    function setTimelineBar(startInt, stopInt) {
        YUI().use('slider', function (Y) {

            //Definimos los valor máximo y mínimo
            var MAX = stopInt;
            var MIN = startInt;

            var MAJOR_STEP = 10;
            var MINOR_STEP = 1;

            var LENGTH = MAX - MIN;
            var MIN_RANGE = 10;

            //Objeto de configuración de ambos sliders
            var config = {
                max: MAX,
                min: MIN,
                majorStep: MAJOR_STEP,
                minorStep: MINOR_STEP,
                value: MAX,
                length: 200,
                clickableRail: false
            };

            //Inicializamos las etiquetas de los sliders
            var sliderDual = Y.one("#sliderDual");

            var startDate = new Date(MIN);
            var stopDate = new Date(MAX);
            sliderDual.one('#txtSliderA').setHTML(startDate.getFullYear() + "." + (startDate.getMonth() + 1) + "." + startDate.getDate());
            sliderDual.one('#txtSliderB').setHTML(stopDate.getFullYear() + "." + (stopDate.getMonth() + 1) + "." + stopDate.getDate());

            //Slider superior
            sliderA = new Y.Slider(config);
            sliderA.setValue(MIN);
            sliderA.render('#sliderA');

            //Slider inferior
            sliderB = new Y.Slider(config).render('#sliderB');

            //Eventos de los sliders

            sliderA.after('thumbMove', function (ev) {
                if (this.getValue() > sliderB.getValue() - MIN_RANGE) {
                    this.setValue(sliderB.getValue() - MIN_RANGE);
                }
                var date = new Date(this.getValue());
                sliderDual.one('#txtSliderA').setHTML(date.toDateString());
                var startTime = Cesium.JulianDate.fromDate(new Date(this.getValue()));
                var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));
                setZoom(startTime, stopTime);
            });
            sliderA.after('slideEnd', function(ev){
                var startTime = Cesium.JulianDate.fromDate(new Date(this.getValue()));
                var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));
                limitData(startTime, stopTime);
            });

            sliderB.after('thumbMove', function (ev) {
                if (this.getValue() < sliderA.getValue() + MIN_RANGE) {
                    this.setValue(sliderA.getValue() + MIN_RANGE);
                }
                var date = new Date(this.getValue());
                sliderDual.one('#txtSliderB').setHTML(date.toDateString());
                var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
                var stopTime = Cesium.JulianDate.fromDate(new Date(this.getValue()));
                setZoom(startTime, stopTime);
            });
            sliderB.after('slideEnd', function(ev){
                var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
                var stopTime = Cesium.JulianDate.fromDate(new Date(this.getValue()));
                limitData(startTime, stopTime);
            });

        });
    }

    /**
     * 全て表示
     */
    function removeTimeSpanBar(){
        var node = document.getElementById("slider");
        for (var i =node.childNodes.length-1; i>=0; i--) {
            node.removeChild(node.childNodes[i]);
        }
        var nodeTxt = document.getElementById("txtSlider");
        for (var i =nodeTxt.childNodes.length-1; i>=0; i--) {
            nodeTxt.removeChild(nodeTxt.childNodes[i]);
        }
    }

    /**
     * タイムスパンのスライダーの設置、及びリスナの設定
     * @param max
     * @param type
     */
    function setTimeSpanBar(max, type) {
        YUI().use('slider', function (Y) {

            //Definimos los valor máximo y mínimo
            var MAX = max;
            var MIN = 1;

            //Objeto de configuración de ambos sliders
            var config = {
                max: MAX,
                min: MIN,
                value: MIN,
                length: 90,
                clickableRail: false
            };

            removeTimeSpanBar();

            //Inicializamos las etiquetas de los sliders
            var sliderSingle = Y.one("#sliderSingle");

            sliderSingle.one('#txtSlider').setHTML(MIN);

            //Slider superior
            var slider = new Y.Slider(config);
            slider.setValue(MIN);
            slider.render('#slider');

            moveSpan();

            //Eventos de los sliders

            slider.after('thumbMove', function (ev) {
                sliderSingle.one('#txtSlider').setHTML(this.getValue());
                intervalDate;
                if(type == "WEEK"){
                    intervalDate = this.getValue() * 7;
                }
                else if(type == "MONTH"){
                    intervalDate = this.getValue() * 30;
                }
                else if(type == "YEAR"){
                    intervalDate = this.getValue() * 365;
                }
                else{
                    intervalDate = this.getValue();
                }
                setIntervals(intervalDate);
            });

            function moveSpan(){
                intervalDate;
                if(type == "WEEK"){
                    intervalDate = 7;
                }
                else if(type == "MONTH"){
                    intervalDate = 30;
                }
                else if(type == "YEAR"){
                    intervalDate = 365;
                }
                else{
                    intervalDate = 1;
                }
                setIntervals(intervalDate);
            }

        });

    }


    /**
     * おりたたみ
     * @param tag
     */
    function collapse(tag){
        document.getElementById(tag + "_expand").style.display = "inline-block";
        document.getElementById(tag + "_collapse").style.display = "none";
        document.getElementById("collapse_content_" + tag).style.display = "none"
        if(tag == "timeline"){
            document.getElementById("collapse_content_search").style.height = document.documentElement.clientHeight - 300 + "px";
        }
    }

    /**
     * 拡げる
     * @param tag
     */
    function expand(tag){
        document.getElementById(tag + "_expand").style.display = "none";
        document.getElementById(tag + "_collapse").style.display = "inline-block";
        document.getElementById("collapse_content_" + tag).style.display = "block"
        if(tag == "timeline"){
            document.getElementById("collapse_content_search").style.height = document.documentElement.clientHeight - 480 + "px";
        }
    }

    /**
     * トピック＆ワード検索
     */
    function select_word(topic, word){
        setTimeout(function(){
            document.body.style.cursor = "wait";
            document.getElementById("searching").style.display = "block";
            document.getElementById("keyword").style.display = "none";
            document.getElementById("close").style.display = "none";
        }, 0);

        var timer = setTimeout(searchWord(), 0);

        function searchWord() {
            var mode = document.getElementsByName("mode");
            var search_mode = (mode[0].checked) ? 0 : 1;

            var katakana = word.replace(/[ぁ-ん]/g, function (s) {
                return String.fromCharCode(s.charCodeAt(0) + 0x60);
            });
            var hiragana = word.replace(/[ァ-ン]/g, function (s) {
                return String.fromCharCode(s.charCodeAt(0) - 0x60);
            });

            //複数選択
            if(searchMultiple){
                if(searchAnd){
                    //同単語の場合
                    if (search_mode == 0) {
                        if(keyword.length == 0 && topicWord.length == 0) {
                            document.getElementById("keyword").innerHTML = "「" + word + "」";
                        }
                        else{
                            document.getElementById("keyword").innerHTML += " and「" + word + "」";
                        }
                        var array = new Array();
                        array[0] = new RegExp(word, "i");
                        array[1] = new RegExp(katakana, "i");
                        array[2] = new RegExp(hiragana, "i");

                        keyword[keyword.length] = array;
                    }
                    //同トピックの同単語の場合
                    else if (search_mode == 1) {
                        if(keyword.length == 0 && topicWord.length == 0) {
                            document.getElementById("keyword").innerHTML = '「' + topic + ': <span class="' + topic + '">' + word + '</span>」';
                        }
                        else{
                            document.getElementById("keyword").innerHTML += ' and「' + topic + ': <span class="' + topic + '">' + word + '</span>」';
                        }
                        var array = new Array();
                        array[0] = new RegExp('class="' + topic + '" title="' + topic + ":" + word + '"', "i");
                        array[1] = new RegExp('class="' + topic + '" title="' + topic + ":" + katakana + '"', "i");
                        array[2] = new RegExp('class="' + topic + '" title="' + topic + ":" + hiragana + '"', "i");

                        keyword[keyword.length] = array;
                    }
                }
                else{
                    //同単語の場合
                    if (search_mode == 0) {
                        if(keyword.length == 0 && topicWord.length == 0) {
                            document.getElementById("keyword").innerHTML = "「" + word + "」";
                        }
                        else{
                            document.getElementById("keyword").innerHTML += " or「" + word + "」";
                        }
                        keyword[keyword.length] = new RegExp(word, "i");
                        keyword[keyword.length] = new RegExp(katakana, "i");
                        keyword[keyword.length] = new RegExp(hiragana, "i");
                    }
                    //同トピックの同単語の場合
                    else if (search_mode == 1) {
                        if(keyword.length == 0 && topicWord.length == 0) {
                            document.getElementById("keyword").innerHTML = '「' + topic + ': <span class="' + topic + '">' + word + '</span>」';
                        }
                        else{
                            document.getElementById("keyword").innerHTML += ' or「' + topic + ': <span class="' + topic + '">' + word + '</span>」';
                        }
                        keyword[keyword.length] = new RegExp('class="' + topic + '" title="' + topic + ":" + word + '"', "i");
                        keyword[keyword.length] = new RegExp('class="' + topic + '" title="' + topic + ":" + katakana + '"', "i");
                        keyword[keyword.length] = new RegExp('class="' + topic + '" title="' + topic + ":" + hiragana + '"', "i");
                    }
                }
            }
            //単独選択
            else{
                keyword = new Array();
                topicWord = new Array();

                //同単語の場合
                if (search_mode == 0) {
                    keyword[0] = new RegExp(word, "i");
                    keyword[1] = new RegExp(katakana, "i");
                    keyword[2] = new RegExp(hiragana, "i");
                    document.getElementById("keyword").innerHTML = "「" + word + "」";
                }
                //同トピックの同単語の場合
                else if (search_mode == 1) {
                    keyword[0] = new RegExp('class="' + topic + '" title="' + topic + ":" + word + '"', "i");
                    keyword[1] = new RegExp('class="' + topic + '" title="' + topic + ":" + katakana + '"', "i");
                    keyword[2] = new RegExp('class="' + topic + '" title="' + topic + ":" + hiragana + '"', "i");
                    document.getElementById("keyword").innerHTML = '「' + topic + ': <span class="' + topic + '">' + word + '</span>」';
                }
            }

            var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
            var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));

            limitData(startTime, stopTime);
        }
    }

    /**
     * フリーキーワード検索
     */
    function search_keyword(){
        setTimeout(function(){
            document.body.style.cursor = "wait";
            document.getElementById("searching").style.display = "block";
            document.getElementById("keyword").style.display = "none";
            document.getElementById("close").style.display = "none";
        }, 0);

        var timer = setTimeout(searchKeyword(), 0);

        function searchKeyword() {
            var inputStr = document.getElementsByName("keyword");
            var inputArray = inputStr[0].value.match(/([^\s]+)/g);
            var mode = document.getElementsByName("search_type");
            var search_mode = (mode[0].checked) ? 0 : 1;

            topicWord = new Array();
            keyword = new Array();

            //And検索
            if (search_mode == 0) {
                document.getElementById("keyword").innerHTML = "「";
                for (var i = 0; i < inputArray.length; i++) {
                    var katakana = inputArray[i].replace(/[ぁ-ん]/g, function (s) {
                        return String.fromCharCode(s.charCodeAt(0) + 0x60);
                    });
                    var hiragana = inputArray[i].replace(/[ァ-ン]/g, function (s) {
                        return String.fromCharCode(s.charCodeAt(0) - 0x60);
                    });
                    keyword[i] = new Array();
                    keyword[i][0] = new RegExp(inputArray[i], "i");
                    keyword[i][1] = new RegExp(katakana, "i");
                    keyword[i][2] = new RegExp(hiragana, "i");
                    if (i != 0) {
                        document.getElementById("keyword").innerHTML += " and ";
                    }
                    document.getElementById("keyword").innerHTML += '「' + inputArray[i] + '」';
                }
                document.getElementById("keyword").innerHTML += "」";
            }
            //OR検索
            else {
                document.getElementById("keyword").innerHTML = "「";
                for (var i = 0; i < inputArray.length; i++) {
                    var katakana = inputArray[i].replace(/[ぁ-ん]/g, function (s) {
                        return String.fromCharCode(s.charCodeAt(0) + 0x60);
                    });
                    var hiragana = inputArray[i].replace(/[ァ-ン]/g, function (s) {
                        return String.fromCharCode(s.charCodeAt(0) - 0x60);
                    });
                    keyword[i * 3] = new RegExp(inputArray[i], "i");
                    keyword[i * 3 + 1] = new RegExp(katakana, "i");
                    keyword[i * 3 + 2] = new RegExp(hiragana, "i");
                    if (i != 0) {
                        document.getElementById("keyword").innerHTML += " or ";
                    }
                    document.getElementById("keyword").innerHTML += '「' + inputArray[i] + '」';
                }
                document.getElementById("keyword").innerHTML += "」";
            }

            var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
            var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));

            limitData(startTime, stopTime);
        }
    }

    /**
     * トピック検索
     * @param topic
     */
    function select_clust(topic){
        setTimeout(function(){
            document.body.style.cursor = "wait";
            document.getElementById("searching").style.display = "block";
            document.getElementById("keyword").style.display = "none";
            document.getElementById("close").style.display = "none";
        }, 0);

        var timer = setTimeout(searchTopic(), 0);

        function searchTopic() {
            var mode = document.getElementsByName("mode_c");
            var search_mode = (mode[0].checked) ? 0 : 1;

            //同トピックが出現する場所の場合
            if (search_mode == 0) {
                //複数検索
                if(searchMultiple) {
                    if(keyword.length == 0){
                        document.getElementById("keyword").innerHTML = '「<span class="V' + topic + '">V' + topic + '</span>含む地点」';
                    }
                    else{
                        if(searchAnd){
                            document.getElementById("keyword").innerHTML += ' and 「<span class="V' + topic + '">V' + topic + '</span>含む地点」';
                        }
                        else{
                            document.getElementById("keyword").innerHTML += ' or 「<span class="V' + topic + '">V' + topic + '</span>含む地点」';
                        }
                    }

                    if(searchAnd){
                        var array = new Array();
                        array[0] = new RegExp('class="V' + topic + '"', "i");
                        keyword[keyword.length] = array;
                    }
                    else{
                        keyword[keyword.length] = new RegExp('class="V' + topic + '"', "i");
                    }

                }
                //単独検索
                else{
                    keyword = new Array();
                    topicWord = new Array();
                    keyword[0] = new RegExp('class="V' + topic + '"', "i");
                    document.getElementById("keyword").innerHTML = '「<span class="V' + topic + '">V' + topic + '</span>」が含まれる地点';
                }

            }
            //同トピックの場所の場合
            else if (search_mode == 1) {
                //複数検索
                if(searchMultiple) {
                    if(keyword.length == 0 && topicWord.length == 0){
                        document.getElementById("keyword").innerHTML = '「<span class="V' + topic + '">V' + topic + '</span>地点」';
                    }
                    else{
                        if(searchAnd){
                            document.getElementById("keyword").innerHTML += ' and 「<span class="V' + topic + '">V' + topic + '</span>地点」';
                        }
                        else{
                            document.getElementById("keyword").innerHTML += ' or 「<span class="V' + topic + '">V' + topic + '</span>地点」';
                        }
                    }
                    topicWord[topicWord.length] = topic;
                }
                //単独検索
                else{
                    keyword = new Array();
                    topicWord = new Array();
                    topicWord[0] = topic;
                    document.getElementById("keyword").innerHTML = '「<span class="V' + topicWord[0] + '">V' + topicWord[0] + '</span>」に分類される地点';
                }
            }

            var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
            var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));

            limitData(startTime, stopTime);
        }
    }

    /**
     * 検索をクリア
     */
    function clearFile(){
        keyword = new Array();
        topicWord = new Array();
        var startTime = Cesium.JulianDate.fromDate(new Date(sliderA.getValue()));
        var stopTime = Cesium.JulianDate.fromDate(new Date(sliderB.getValue()));
        document.getElementById("keyword").style.display = "none";
        document.getElementById("close").style.display = "none";

        limitData(startTime, stopTime);
    }

    function resetSearch(name){
        if(keyword.length > 0 || topicWord.length > 0) {
            swal({
                    title: "",
                    text: "これまでの検索結果はリセットされますがよろしいですか？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "リセット",
                    cancelButtonText: "キャンセル",
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if(isConfirm) {
                        doClear();
                    }
                    else{
                        var node = document.getElementsByName(name);
                        for(var i = 0; i < node.length; i++){
                            if(!node[i].checked){
                                node[i].checked = true;
                                break;
                            }
                        }
                    }
                }
            );
        }
        else{
            doClear();
        }


        /**
         * 検索結果のリセット実行
         */
        function doClear() {
            clearFile();
            var mode = document.getElementsByName("mode_s");
            var search_mode = (mode[0].checked) ? 0 : 1;
            var type = document.getElementsByName("search_type_s");
            var search_type = (type[0].checked) ? 0 : 1;

            //単独選択の場合
            if (search_mode == 0) {
                searchMultiple = false;
                type[0].disabled = true;
                type[1].disabled = true;
            }
            //複数選択の場合
            else {
                searchMultiple = true;
                type[0].disabled = false;
                type[1].disabled = false;

            }

            //ANDの場合
            if (search_type == 0) {
                searchAnd = true;
            }
            //ORの場合
            else {
                searchAnd = false;
            }
        }
    }

    /**
     * 検索結果を表示
     * @param count
     */
    function search(count){
        setTimeout(function(){
            document.getElementById("result").innerHTML = count + "件見つかりました";
            document.getElementById("searching").style.display = "none";
            if(keyword.length != 0 || topicWord.length != 0){
                document.getElementById("keyword").style.display = "block";
                document.getElementById("close").style.display = "block";
            }
            document.body.style.cursor = "default";
        },0);
        createTable(0, 0, false);
    }

    function subwindow(url){
        window.open(url, "topic chart", "width = 600, height=400, scrollbars=yes");
    }

    function newwindow() {
        var content = document.getElementById("cesium-infoBox-iframe").contentWindow.document.getElementById("cesium-infoBox-description").innerHTML;
        var idnumber = document.getElementById("cesium-infoBox-title").innerHTML;
        var nwin = window.open("", "Note-"+idnumber,"width=400,height=500,menubar=no, toolbar=no, scrollbars=yes");
        nwin.document.open();
        nwin.document.write("<HTML><HEAD>");
        nwin.document.write("<link rel='stylesheet' href='css/init.css' type='text/css' />");
        nwin.document.write("<link rel='stylesheet' href='css/style.css' type='text/css' />");

        var javascriptLink = nwin.document.createElement("script");
        javascriptLink.type = "text/javascript";
        javascriptLink.innerHTML = '\
                    function tp(obj){\
                    var topic = obj.className;\
                    var term = obj.innerHTML;\
                    window.opener.select_word(topic, term);\
                }';
        nwin.document.head.appendChild(javascriptLink);

        nwin.document.write("<TITLE>Note-" + idnumber + "</TITLE>");
        nwin.document.write("</HEAD>");
        nwin.document.writeln("<BODY style='overflow-y:scroll; height:100%;'>");
        nwin.document.writeln("<div class='newwindow'>");
        nwin.document.write(content);
        nwin.document.writeln("</div>");
        nwin.document.write("</BODY></HTML>");
        nwin.document.close();
    }

    function resetConfirm() {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            }
        );
    }

    /**
     * fade out
     * @param gazouId
     * @param opa
     * @constructor
     */
    function fadeOut(id,opa){
        if (0 <= opa){
            document.getElementById(id).style.filter = "alpha(opacity:" + opa + ")"; // IE のソース
            document.getElementById(id).style.opacity = opa / 100; //Mozilla Firefoxのソース
            opa -= 10;
            setTimeout("fadeOut('" + id + "'," + opa + ")", 100);
        }
        else{
            document.getElementById(id).style.display = 'none';
        }
    }

    /*----------------
     リスナー各種
     ----------------------- */

    /**
     * カメラのチェンジリスナ
     */
    function setCameraListener(){
        var a = 1 -  (Math.pow(10, 6) / (7 * Math.pow(10, 6)));
        var intervalHandle = setInterval(function() {
            var camera = viewerMain.scene.camera,
                store = {  position: camera.position.clone()
                };
            var cart = Cesium.Ellipsoid.WGS84.cartesianToCartographic(store.position);
            var altitude = a * cart.height + Math.pow(10, 6);
            if(altitude > 7 * Math.pow(10, 6)){
                altitude = 7 * Math.pow(10, 6);
            }
            viewerMini.camera.setView({
                position : Cesium.Cartesian3.fromDegrees(Cesium.Math.toDegrees(cart.longitude), Cesium.Math.toDegrees(cart.latitude), altitude)
            });
        }, 100);
    }

    /**
     * billboardクリックイベントリスナ
     */
    function setClickEvent(){
        var handler = new Cesium.ScreenSpaceEventHandler(viewerMain.scene.canvas);
        handler.setInputAction(function(click) {

            var pickedObject = viewerMain.scene.pick(click.position);
            if (Cesium.defined(pickedObject) && (pickedObject != "")) {
                for(var i = 0; i < entitiesMain.length; i++){
                    if(entitiesMain[i].id == pickedObject.id.id){
                        createTable(i, 3, true);
                        break;
                    }
                }
            }
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    }

    /**
     * タイムラインの行クリックリスナ
     */
    function tableClick(obj){
        var index = obj.id;
        viewerMain.selectedEntity = entitiesMain[index];
        var currentItem = document.getElementsByClassName("list-current");
        if(currentItem.length > 0) {
            currentItem[0].className = "list";
        }
        obj.className = "list-current";
    }

    function listBack(current){
        var table = document.getElementById("list_table");
        var node = table.getElementsByTagName("tr")[0];
        if(-1 < parseInt(node.id) - 1) {
            createTable(parseInt(node.id) - 1, 6, current);
        }
    }

    function listNext(current){
        var table = document.getElementById("list_table");
        var nodes = table.getElementsByTagName("tr");
        var node = nodes[nodes.length - 1];
        if(parseInt(node.id) + 1 < entitiesMain.length) {
            createTable(parseInt(node.id) + 1, 0, current);
        }
    }

    /**
     * キーイベント
     */
    function keyevent(e){

        // InternetExplorer 用
        if (!e)	e = window.event;

        if(e.keyCode == 38){//UPkey
            upkey();
        }
        else if(e.keyCode == 40){//DOWNkey
            downkey();
        }
    };

    function upkey(){
        var current;
        if(document.getElementsByClassName("list-current")[0]){
            var table = document.getElementById("list_table");
            var nodes = table.getElementsByTagName("tr");
            for(var i = 0; i < nodes.length; i++){
                if(nodes[i].className == "list-current"){
                    current = i - 1;
                    break;
                }
            }
        }

        if(current >= 0){
            document.getElementsByClassName("list")[current].click();
        }
        else{
            listBack(true);
        }
    }

    function downkey(){
        var current;
        var table = document.getElementById("list_table");
        var nodes = table.getElementsByTagName("tr");
        if(document.getElementsByClassName("list-current")[0]){
            for(var i = 0; i < nodes.length; i++){
                if(nodes[i].className == "list-current"){
                    current = i;
                    break;
                }
            }
        }

        if(current < nodes.length - 1){
            document.getElementsByClassName("list")[current].click();
        }
        else{
            listNext(true);
        }
    }

    /**
     * animation startボタン
     */
    function toggleStart(){
        viewerMain.clock.shouldAnimate = !viewerMain.clock.shouldAnimate;
        viewerMain.clock.multiplier = 3600 * 24;
    }

    /**
     * 巻き戻し
     */
    function toggleSlow(){
        viewerMain.clock.shouldAnimate = true;
        if(3600 * 24 <= viewerMain.clock.multiplier) {
            viewerMain.clock.multiplier = -3600 * 24;
        }
        else{
            viewerMain.clock.multiplier = Math.abs(viewerMain.clock.multiplier) * -2;
        }
    }

    /**
     * 早送り
     */
    function toggleRapid(){
        viewerMain.clock.shouldAnimate = true;
        if(viewerMain.clock.multiplier < 3600 * 24) {
            viewerMain.clock.multiplier = 3600 * 24 * 2;
        }
        else{
            viewerMain.clock.multiplier = Math.abs(viewerMain.clock.multiplier) * 2;
        }
    }


    /**
     * 時計の変化を取得するリスナ
     * @param clock
     */
    function tickListener(clock){
        var currentTime = clock.currentTime.toString();
        var obj = new Date(Date.parse( currentTime ));
        //obj.getMonth() + 1にする
        document.getElementById("currentTime").innerHTML = obj.getFullYear() + "." + (obj.getMonth() + 1) + "." + obj.getDate();
        if(clock.shouldAnimate){
            if(document.getElementById("playBtn")) {
                var newElement = document.createElement("img");
                newElement.setAttribute("id", "pauseBtn");
                newElement.setAttribute("src", "res/pause.png");
                var oldElement = document.getElementById("playBtn");
                oldElement.parentNode.replaceChild(newElement, oldElement);
            }
        }
        else{
            if(document.getElementById("pauseBtn")) {
                var newElement = document.createElement("img");
                newElement.setAttribute("id", "playBtn");
                newElement.setAttribute("src", "res/play.png");
                var oldElement = document.getElementById("pauseBtn");
                oldElement.parentNode.replaceChild(newElement, oldElement);
            }
        }
    }

}());